window.addEventListener("load", drawAcs);

function createSvgElement(tag) {
    return document.createElementNS("http://www.w3.org/2000/svg", tag);
}

/* [
Copyright © 2020 Xah Lee

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

URL: SVG Circle Arc http://xahlee.info/js/svg_circle_arc.html
Version 2019-06-19

] */

const cos = Math.cos;
const sin = Math.sin;
const Pi = Math.PI;

const f_matrix_times = (([[a, b], [c, d]], [x, y]) => [a * x + b * y, c * x + d * y]);
const f_rotate_matrix = (x => [[cos(x), -sin(x)], [sin(x), cos(x)]]);
const f_vec_add = (([a1, a2], [b1, b2]) => [a1 + b1, a2 + b2]);
const angleRad = (a) => a * Pi / 180;

const f_svg_ellipse_arc = (([cx, cy], [rx, ry], [t1, delta], theta) => {
    /* [
    returns a SVG path element that represent a ellipse.
    cx,cy → center of ellipse
    rx,ry → major minor radius
    t1 → start angle, in radian.
    delta → angle to sweep, in radian. positive.
    theta → rotation on the whole, in radian
    URL: SVG Circle Arc http://xahlee.info/js/svg_circle_arc.html
    Version 2019-06-19
     ] */
    delta = delta % (2 * Pi);
    const rotMatrix = f_rotate_matrix(theta);
    const [sX, sY] = (f_vec_add(f_matrix_times(rotMatrix, [rx * cos(t1), ry * sin(t1)]), [cx, cy]));
    const [eX, eY] = (f_vec_add(f_matrix_times(rotMatrix, [rx * cos(t1 + delta), ry * sin(t1 + delta)]), [cx, cy]));
    const fA = ((delta > Pi) ? 1 : 0);
    const fS = ((delta > 0) ? 1 : 0);
    const path_2wk2r = createSvgElement("path");
    path_2wk2r.setAttribute("d", `M ${sX} ${sY} A ${[rx, ry, theta / (2 * Pi) * 360, fA, fS, eX, eY].join(" ")}`);
    return path_2wk2r;
});

function drawArc(x, y, r, angle1, lineWidth) {
    const arc = f_svg_ellipse_arc([x, y], [r, r], [0, angleRad(angle1)], angleRad(-90));
    arc.setAttribute("stroke-width", `${lineWidth}px`);
    arc.setAttribute("stroke", "#eee");
    return arc;
}


function drawAcs() {
    const element = document.getElementById("put");
    if (!element) {
        return;
    }

    const arcs = createSvgElement("svg");

    const size = 60;
    const lineWidth = 5;
    const gap = 1;
    const num = 10;

    const radius = (size - lineWidth) / 2; // - lineWidth;

    const step = size + gap * 2;
    const angleStep = 360 / (num * num);

    const sWidth = step * num;
    const sHeight = step * num;


    arcs.setAttribute("fill", "none");
    arcs.setAttribute("viewBox", `0 0 ${sWidth} ${sHeight}`);

    let angle1 = angleStep;
    let y = step / 2;
    for (let i = 0; i < num; i++) {
        let x = step / 2;
        for (let j = 0; j < num; j++) {
            if (i === num-1 && j === num-1) {
                angle1 = 359.99;
            }
            const arc = drawArc(x, y, radius, angle1, lineWidth);
            arcs.appendChild(arc);
            x += step;
            angle1 += angleStep;

        }
        y += step;
    }

    element.appendChild(arcs);
}
